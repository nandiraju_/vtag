$('body').bind('show-menu-screen', function(e, data) {
	$("#fullscreen_stage").load("screens/menu.html");
	showFullScreen();
});

$('body').bind('show-home-screen', function(e, data) {
	$("#fullscreen_stage").load("screens/home.html");
});

$('body').bind('hide-menu-screen', function(e, data) {
	$('#fullscreenElement').removeClass('open');
});

$('body').bind('vertical-screen', function(e, data) {
	$("#fullscreen_stage").load("screens/vertical_screen.html");
	showFullScreen();
});

$('body').bind('show-login-screen', function(e, data) {
	$("#title_text").html("LOGIN");
	$("#stage").load("screens/login.html");
});

$('body').bind('show-register-screen', function(e, data) {
	$("#title_text").html("REGISTER");
	$("#stage").load("screens/register.html");
});

$('body').bind('show-identity-screen', function(e, data) {
	$("#title_text").html("IDENTITY");
	$("#stage").load("screens/identity.html");
});

