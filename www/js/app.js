// ------------------- PROGRESS BARS -------------------------------
$(document).ajaxStart(function() {
	NProgress.start();
});

$(document).ajaxStop(function() {
	NProgress.done();
});

// ------------------- EVENTS ON ELEMENTS -------------------------------
$(document.body).on('click', '.navigation-item', function() {
	var api_url = $(this).attr("data-api-url");
	document.location.href = api_url;
});

// ------------------- FULLSCREEN ITEMS -------------------------------
function showFullScreen() {
	$('#fullscreenElement').addClass('open');
}

$('.close').on('click', function(event) {
	$('#fullscreenElement').removeClass('open');
});

// ------------------- SCROLLING FIX -------------------------------
function fixScrolling() {
	var stuff = {};
	$('#center-body').on('touchstart', stuff, function(e) {
		e.data.max = this.scrollHeight - this.offsetHeight;
		e.data.y = e.originalEvent.pageY;
	}).on('touchmove', stuff, function(e) {
		var dy = e.data.y - e.originalEvent.pageY;
		// if scrolling up and at the top, or down and at the bottom
		if ((dy < 0 && this.scrollTop < 1) || (dy > 0 && this.scrollTop >= e.data.max)) {
			e.preventDefault();
		};
	});
}

$(document.body).on('click', '.show-screen', function() {
	var api_url = $(this).attr("data-api-url");
	document.location.href = api_url;
});

// ------------------------------ SCREENS ------------------------------
function showMenu() {
}

