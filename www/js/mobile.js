var nfcMessage = [ndef.textRecord("")];
document.addEventListener('deviceready', deviceReady, false);

function deviceReady() {
}

function nfcHandler(nfcEvent) {
	var tag = nfcEvent.tag;
	var ndefMessage = tag.ndefMessage;
	var payload = nfc.bytesToString(ndefMessage[0].payload);

	unRegister();
	//search(payload.toString());
	$('#id').val(payload);
	$('#save_button').click();

	/*var data = JSON.parse(LS.get_data('data'));
	 var event = $.grep(data, function(event, event_index) {
	 return event.id == payload;
	 });
	 alert("SEARCH USING PAYLOAD = " + JSON.stringify(event));
	 LS.set_data('search_item', event);
	 $('body').trigger('vertical-screen');*/
};

function readData() {
	nfc.addNdefListener(nfcHandler, function() {
		//alert("Success, listener added. You can now scan a tag.");
		$("#scan-ready").html("Ready to scan <b>Vaccine Tag</b>");
		$("#scan-ready").addClass("animated pulse infinite");
	}, function(error) {
		alert("Adding the listener failed.");
	});
}

function unRegister() {
	nfc.removeNdefListener(nfcHandler, // this must be the same as the function above
	function() {
		console.log("Success, the listener has been removed.");
		$("#scan-ready").html("");
	}, function(error) {
		alert("Removing the listener failed");
	});
}

function writeData() {
	nfc.write(nfcMessage, function() {
		//alert("Write Success");
		$("#register-header").html("<b class='animated pulse infinite'>TAG</b>&nbsp;SUCCESS");
	}, function(error) {
		alert("Error adding NDEF listener\n" + JSON.stringify(error));
	});
}

// REGISTER EVENTS

$('body').bind('write-nfc-data', function(e, data) {
	var data = data || e.data;
	alert("Writing " + data);
	nfcMessage = [ndef.textRecord(data)];
	writeData();
});

$('body').bind('enable-nfc-read', function(e, data) {
	//var data = data || e.data;
	//nfcMessage = data;
	readData();
});
